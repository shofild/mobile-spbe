package gnews.e_jobdesk.home;

import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.ScrollView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import gnews.e_jobdesk.MainActivity;
import gnews.e_jobdesk.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class I0000000000temOneFragment extends Fragment{
    private BarChart chart;
    float barWidth;
    float barSpace;
    float groupSpace;
    ArrayList xVals = new ArrayList();
    ArrayList yVals1 = new ArrayList();
    ArrayList yVals2 = new ArrayList();

    RecyclerView recyclerView;
    CustomAdapter customAdapter;
    List<Model> Datalist;

    ApiInterface mApiInterface;

    public static ItemOneFragment newInstance(){
        ItemOneFragment fragment = new ItemOneFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_action1, container,false);
        getActivity().setTitle("home");
        ScrollView scrollView = view.findViewById(R.id.scrollVV);
        scrollView.setFocusableInTouchMode(true);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        mApiInterface = ApiClient.getClient(getActivity()).create(ApiInterface.class);

        drawChart();

        final BottomNavigationView bottomNavigationView = MainActivity.bottomNavigationView;

        Datalist =new ArrayList<>();
        customAdapter = new CustomAdapter(view.getContext(), Datalist);

        final TranslateAnimation animation = new TranslateAnimation(0,0,0,view.getHeight());
        animation.setDuration(1000);
        animation.setFillAfter(true);

        recyclerView = view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mlayoutManager = new GridLayoutManager(view.getContext(), 3);
        recyclerView.setLayoutManager(mlayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(3,dpToPx(10),true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(customAdapter);

        prepareData();
        return view;
    }

    private void prepareData() {
        String[] personNames = new String[]{"Human Resource","Management", "Merchandise","Creative", "Operational", "Editorial", "Technology"};

        int[] personImages = new int[]{R.drawable.hr,R.drawable.manajemen,
                R.drawable.merchaindes,R.drawable.creative, R.drawable.operasional, R.drawable.redaksi, R.drawable.technology};


        for (int x = 0 ; x<personNames.length;x++){
            Model a = new Model(1,personImages[x],personNames[x]);
            Datalist.add(a);
        }
        customAdapter.notifyDataSetChanged();

    }

    private void drawChart(){
        barWidth = 0.3f;
        barSpace = 0f;
        groupSpace = 0.4f;

        chart = (BarChart)view.findViewById(R.id.barChart);
        chart.getDescription().setEnabled(false);
        chart.setPinchZoom(true);
        chart.setScaleEnabled(false);
        chart.setDrawBarShadow(false);
        chart.setDrawGridBackground(false);
        chart.animateY(3000);

        //Dummy Data
        int groupCount = 6;

        getChartData();

        //y label
        xVals.add("IT");
        xVals.add("Merc");
        xVals.add("News");
        xVals.add("Report");
        xVals.add("Analys");
        xVals.add("PG");


        yVals1.add(new BarEntry(1, (float) 6));
        yVals2.add(new BarEntry(1, (float) 2));
        yVals1.add(new BarEntry(2, (float) 1));
        yVals2.add(new BarEntry(2, (float) 4));
        yVals1.add(new BarEntry(3, (float) 5));
        yVals2.add(new BarEntry(3, (float) 9));
        yVals1.add(new BarEntry(4, (float) 2));
        yVals2.add(new BarEntry(4, (float) 7));
        yVals1.add(new BarEntry(5, (float) 13));
        yVals2.add(new BarEntry(5, (float) 2));
        yVals1.add(new BarEntry(6, (float) 5));
        yVals2.add(new BarEntry(6, (float) 6));

        //Draw
        BarDataSet set1, set2;
        set1 = new BarDataSet(yVals1, "RESULT");
        set1.setColor(Color.rgb(25,100,22));
        set2 = new BarDataSet(yVals2, "EXPECTED");
        set2.setColor(Color.RED);
        BarData data = new BarData(set1, set2);
        data.setValueFormatter(new LargeValueFormatter());
        chart.setData(data);
        chart.getBarData().setBarWidth(barWidth);
        chart.getXAxis().setAxisMinimum(0);
        chart.getXAxis().setAxisMaximum(0 + chart.getBarData().getGroupWidth(groupSpace, barSpace) * groupCount);
        chart.groupBars(0, groupSpace, barSpace);
        chart.getData().setHighlightEnabled(false);
        chart.invalidate();

        //Indicator
        Legend l = chart.getLegend();
        l.setDrawInside(true);
        l.setYEntrySpace(5f);
        l.setXEntrySpace(5f);
        l.setTextSize(8f);
        l.setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);



        //X-axis
        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setCenterAxisLabels(true);
        xAxis.setDrawGridLines(false);
        xAxis.setAxisMaximum(6);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(xVals));
        //Y-axis
        chart.getAxisRight().setEnabled(false);
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(true);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f);
    }

    private void getChartData() {
        Call<ResponseBody> call = mApiInterface.getDepartmentTask();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    JSONObject jsonObject = new JSONObject(response.body().string());
                    JSONArray jsonArray = new JSONArray(jsonObject.getString("values"));
                    Log.e("RESPONSE", "onResponse: "+jsonArray.get(0));
                    JSONObject jsonObject1 = new JSONObject(jsonArray.get(0).toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
