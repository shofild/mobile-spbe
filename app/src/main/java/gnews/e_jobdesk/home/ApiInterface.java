package gnews.e_jobdesk.home;

import java.util.List;

import gnews.e_jobdesk.model.DomainItem;
import gnews.e_jobdesk.model.InternalResponse;
import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("/task/department") Call<ResponseBody> getDepartmentTask();

    @GET("spbe-evaluasi/api/internal")
    Single<InternalResponse> getDomain();
}
