package gnews.e_jobdesk.model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class InternalResponse{

	@SerializedName("domain")
	private List<DomainItem> domain;

	@SerializedName("indikator")
	private List<IndikatorItem> indikator;

	@SerializedName("aspek")
	private List<AspekItem> aspek;

	@SerializedName("count")
	private int count;

	public void setDomain(List<DomainItem> domain){
		this.domain = domain;
	}

	public List<DomainItem> getDomain(){
		return domain;
	}

	public void setIndikator(List<IndikatorItem> indikator){
		this.indikator = indikator;
	}

	public List<IndikatorItem> getIndikator(){
		return indikator;
	}

	public void setAspek(List<AspekItem> aspek){
		this.aspek = aspek;
	}

	public List<AspekItem> getAspek(){
		return aspek;
	}

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	@Override
 	public String toString(){
		return 
			"InternalResponse{" + 
			"domain = '" + domain + '\'' + 
			",indikator = '" + indikator + '\'' + 
			",aspek = '" + aspek + '\'' + 
			",count = '" + count + '\'' + 
			"}";
		}
}