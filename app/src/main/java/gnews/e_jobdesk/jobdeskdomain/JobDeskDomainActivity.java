package gnews.e_jobdesk.jobdeskdomain;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import gnews.e_jobdesk.BaseActivity;
import gnews.e_jobdesk.R;
import gnews.e_jobdesk.home.ApiClient;
import gnews.e_jobdesk.home.ApiInterface;
import gnews.e_jobdesk.jobdeskaspect.JobDeskAspectActivity;
import gnews.e_jobdesk.jobdeskaspect.JobDeskAspectAdapter;
import gnews.e_jobdesk.model.DomainItem;
import gnews.e_jobdesk.model.InternalResponse;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class JobDeskDomainActivity extends BaseActivity implements JobDeskDomainAdapter.Callback{

    JobDeskDomainAdapter jobDeskDomainAdapter;
    ApiInterface apiInterface;
    CompositeDisposable disposable = new CompositeDisposable();
    List<DomainItem> domainItemList;

    @BindView(R.id.rv_domain)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_desk_domain);
        ButterKnife.bind(this);
        apiInterface = ApiClient.getClient(getApplicationContext()).create(ApiInterface.class);

        jobDeskDomainAdapter = new JobDeskDomainAdapter(new ArrayList<>(), this);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(jobDeskDomainAdapter);
        jobDeskDomainAdapter.setCallback(this);

        getDomain();
    }

    private void getDomain() {
        disposable.add(
                apiInterface
                        .getDomain()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(new Function<InternalResponse, InternalResponse>() {
                            @Override
                            public InternalResponse apply(InternalResponse datas) throws Exception {
                                return datas;
                            }
                        })
                        .subscribeWith(new DisposableSingleObserver<InternalResponse>() {
                            @Override
                            public void onSuccess(InternalResponse datas) {
                                domainItemList = datas.getDomain();
                                jobDeskDomainAdapter.addItems(datas.getDomain());
                                jobDeskDomainAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onError(Throwable e) {
                                showMessage("Tidak terhubung server");
                                Log.d("ERROR", "onError: " + e.getMessage());
                                showError(e);
                            }
                        }));
    }

    @Override
    protected void onDestroy() {
        disposable.dispose();
        super.onDestroy();
    }

    @Override
    public void onItemLocationListClick(int position) {
        Intent intent = new Intent(getApplicationContext(), JobDeskAspectActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
