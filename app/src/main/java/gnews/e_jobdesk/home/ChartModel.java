package gnews.e_jobdesk.home;

public class ChartModel {
    private String name;
    private String doneTask;
    private String toDoTask;

    public ChartModel(String name, String doneTask, String toDoTask) {
        this.name = name;
        this.doneTask = doneTask;
        this.toDoTask = toDoTask;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDoneTask() {
        return doneTask;
    }

    public void setDoneTask(String doneTask) {
        this.doneTask = doneTask;
    }

    public String getToDoTask() {
        return toDoTask;
    }

    public void setToDoTask(String toDoTask) {
        this.toDoTask = toDoTask;
    }
}
