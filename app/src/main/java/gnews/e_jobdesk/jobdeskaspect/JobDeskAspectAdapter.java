package gnews.e_jobdesk.jobdeskaspect;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.ButterKnife;
import gnews.e_jobdesk.BaseViewHolder;
import gnews.e_jobdesk.R;
import gnews.e_jobdesk.jobdeskdomain.JobDeskDomainAdapter;
import gnews.e_jobdesk.model.AspekItem;
import gnews.e_jobdesk.model.DomainItem;

public class JobDeskAspectAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private JobDeskAspectAdapter.Callback mCallback;
    private List<AspekItem> mDataList;
    private List<AspekItem> mDataListDefault;
    private String mType;
    Context context;

    public JobDeskAspectAdapter(List<AspekItem> datas, Context context) {
        mDataList = datas;
        this.context = context;
    }

    public void setCallback(JobDeskAspectAdapter.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new JobDeskAspectAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_aspect, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new JobDeskAspectAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDataList != null && mDataList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mDataList != null && mDataList.size() > 0) {
            return mDataList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<AspekItem> dataList) {
        mDataList.clear();
        mDataList.addAll(dataList);
        mDataList = dataList;
        notifyDataSetChanged();
    }

    public interface Callback {
        void onItemLocationListClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {

        TextView tvAspekName;
        TextView tvAspekDesc;


        public ViewHolder(View itemView) {
            super(itemView);
            tvAspekName = itemView.findViewById(R.id.tv_aspect_name);
            tvAspekDesc = itemView.findViewById(R.id.tv_aspect_desc);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            AspekItem item = mDataList.get(position);
            Log.d("Debug",mDataList.toString());

            tvAspekName.setText(item.getAspekName());
            tvAspekDesc.setText(item.getAspekDescription());

            itemView.setOnClickListener(v->{
                if (mCallback != null){
                    mCallback.onItemLocationListClick(position);
                }
            });

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {


        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

    }
}
