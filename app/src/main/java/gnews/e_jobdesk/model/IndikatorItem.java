package gnews.e_jobdesk.model;

import com.google.gson.annotations.SerializedName;

public class IndikatorItem{

	@SerializedName("indikator_description")
	private String indikatorDescription;

	@SerializedName("indikator_internal_level")
	private Object indikatorInternalLevel;

	@SerializedName("updated_at")
	private Object updatedAt;

	@SerializedName("indikator_eksternal_level")
	private Object indikatorEksternalLevel;

	@SerializedName("indikator_question")
	private String indikatorQuestion;

	@SerializedName("order_number")
	private int orderNumber;

	@SerializedName("indikator_weight_of_aspek")
	private double indikatorWeightOfAspek;

	@SerializedName("created_at")
	private Object createdAt;

	@SerializedName("indikator_name")
	private String indikatorName;

	@SerializedName("id")
	private int id;

	@SerializedName("indikator_aspek_id")
	private int indikatorAspekId;

	@SerializedName("indikator_weight")
	private float indikatorWeight;

	public void setIndikatorDescription(String indikatorDescription){
		this.indikatorDescription = indikatorDescription;
	}

	public String getIndikatorDescription(){
		return indikatorDescription;
	}

	public void setIndikatorInternalLevel(Object indikatorInternalLevel){
		this.indikatorInternalLevel = indikatorInternalLevel;
	}

	public Object getIndikatorInternalLevel(){
		return indikatorInternalLevel;
	}

	public void setUpdatedAt(Object updatedAt){
		this.updatedAt = updatedAt;
	}

	public Object getUpdatedAt(){
		return updatedAt;
	}

	public void setIndikatorEksternalLevel(Object indikatorEksternalLevel){
		this.indikatorEksternalLevel = indikatorEksternalLevel;
	}

	public Object getIndikatorEksternalLevel(){
		return indikatorEksternalLevel;
	}

	public void setIndikatorQuestion(String indikatorQuestion){
		this.indikatorQuestion = indikatorQuestion;
	}

	public String getIndikatorQuestion(){
		return indikatorQuestion;
	}

	public void setOrderNumber(int orderNumber){
		this.orderNumber = orderNumber;
	}

	public int getOrderNumber(){
		return orderNumber;
	}

	public void setIndikatorWeightOfAspek(double indikatorWeightOfAspek){
		this.indikatorWeightOfAspek = indikatorWeightOfAspek;
	}

	public double getIndikatorWeightOfAspek(){
		return indikatorWeightOfAspek;
	}

	public void setCreatedAt(Object createdAt){
		this.createdAt = createdAt;
	}

	public Object getCreatedAt(){
		return createdAt;
	}

	public void setIndikatorName(String indikatorName){
		this.indikatorName = indikatorName;
	}

	public String getIndikatorName(){
		return indikatorName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setIndikatorAspekId(int indikatorAspekId){
		this.indikatorAspekId = indikatorAspekId;
	}

	public int getIndikatorAspekId(){
		return indikatorAspekId;
	}

	public void setIndikatorWeight(float indikatorWeight){
		this.indikatorWeight = indikatorWeight;
	}

	public float getIndikatorWeight(){
		return indikatorWeight;
	}

	@Override
 	public String toString(){
		return 
			"IndikatorItem{" + 
			"indikator_description = '" + indikatorDescription + '\'' + 
			",indikator_internal_level = '" + indikatorInternalLevel + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",indikator_eksternal_level = '" + indikatorEksternalLevel + '\'' + 
			",indikator_question = '" + indikatorQuestion + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",indikator_weight_of_aspek = '" + indikatorWeightOfAspek + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",indikator_name = '" + indikatorName + '\'' + 
			",id = '" + id + '\'' + 
			",indikator_aspek_id = '" + indikatorAspekId + '\'' + 
			",indikator_weight = '" + indikatorWeight + '\'' + 
			"}";
		}
}