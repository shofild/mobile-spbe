package gnews.e_jobdesk.jobdesk;

public class Jobdesk {
    private String mTitle;
    private String mDesc;
    private String mDeadline;

    public Jobdesk() {

    }

    public Jobdesk(String title, String deadline) {
        mTitle = title;
        mDeadline = deadline;
    }

    public Jobdesk(String title, String desc, String deadline) {
        mTitle = title;
        mDesc = desc;
        mDeadline = deadline;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDesc() {
        return mDesc;
    }

    public void setDesc(String desc) {
        mDesc = desc;
    }

    public String getDeadline() {
        return mDeadline;
    }

    public void setDeadline(String deadline) {
        mDeadline = deadline;
    }
}
