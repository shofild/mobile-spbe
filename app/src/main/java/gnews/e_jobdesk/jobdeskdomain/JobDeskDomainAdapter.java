package gnews.e_jobdesk.jobdeskdomain;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.ButterKnife;
import gnews.e_jobdesk.BaseViewHolder;
import gnews.e_jobdesk.R;
import gnews.e_jobdesk.model.DomainItem;
import gnews.e_jobdesk.model.InternalResponse;

public class JobDeskDomainAdapter extends RecyclerView.Adapter<BaseViewHolder> {
    public static final int VIEW_TYPE_EMPTY = 0;
    public static final int VIEW_TYPE_NORMAL = 1;

    private JobDeskDomainAdapter.Callback mCallback;
    private List<DomainItem> mDataList;
    private List<DomainItem> mDataListDefault;
    private String mType;
    Context context;

    public JobDeskDomainAdapter(List<DomainItem> datas, Context context) {
        mDataList = datas;
        this.context = context;
    }

    public void setCallback(JobDeskDomainAdapter.Callback callback) {
        mCallback = callback;
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                return new JobDeskDomainAdapter.ViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_domain, parent, false));
            case VIEW_TYPE_EMPTY:
            default:
                return new JobDeskDomainAdapter.EmptyViewHolder(
                        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mDataList != null && mDataList.size() > 0) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public int getItemCount() {
        if (mDataList != null && mDataList.size() > 0) {
            return mDataList.size();
        } else {
            return 1;
        }
    }

    public void addItems(List<DomainItem> dataList) {
        mDataList.clear();
        mDataList.addAll(dataList);
        mDataList = dataList;
        notifyDataSetChanged();
    }

    public interface Callback {
        void onItemLocationListClick(int position);
    }

    public class ViewHolder extends BaseViewHolder {

        TextView tvDomainName;
        TextView tvDomainDesc;


        public ViewHolder(View itemView) {
            super(itemView);
            tvDomainName = itemView.findViewById(R.id.tv_domain_name);
            tvDomainDesc = itemView.findViewById(R.id.tv_domain_desc);
        }

        protected void clear() {

        }

        public void onBind(int position) {
            super.onBind(position);

            DomainItem item = mDataList.get(position);
            Log.d("Debug",mDataList.toString());

            tvDomainName.setText(item.getDomainName());
            tvDomainDesc.setText(item.getDomainDescription());

            itemView.setOnClickListener(v->{
                if (mCallback != null){
                    mCallback.onItemLocationListClick(position);
                }
            });

        }
    }

    public class EmptyViewHolder extends BaseViewHolder {


        public EmptyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        protected void clear() {

        }

    }
}
