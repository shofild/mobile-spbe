package gnews.e_jobdesk.jobdesk;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import gnews.e_jobdesk.R;

public class ItemTwoFragment extends Fragment{
    View v;
    private RecyclerView myRecyclerView;
    private List<Jobdesk> listJobdesk;

    public ItemTwoFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_action2, container,false);
        getActivity().setTitle("jobdesk");
        myRecyclerView = (RecyclerView) v.findViewById(R.id.rv_jobdesk);
        JobdeskAdapter jobdeskAdapter = new JobdeskAdapter(v.getContext().getApplicationContext(),listJobdesk);
        myRecyclerView.setHasFixedSize(true);
        myRecyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        myRecyclerView.setItemAnimator(new DefaultItemAnimator());
        myRecyclerView.setAdapter(jobdeskAdapter);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listJobdesk = new ArrayList<>();
        listJobdesk.add(new Jobdesk("Kabupaten Banggai","1/11/2019"));
        listJobdesk.add(new Jobdesk("Kabupaten Banggai Kepulauan","1/11/2019"));
        listJobdesk.add(new Jobdesk("Kabupaten Dompu","1/11/2019"));
        listJobdesk.add(new Jobdesk("Kabupaten Rokan Hulu","1/11/2019"));
        listJobdesk.add(new Jobdesk("Kabupaten Sumbawa","1/11/2019"));
        listJobdesk.add(new Jobdesk("Kota Depok","1/11/2019"));
        listJobdesk.add(new Jobdesk("Lembaga BNPT","1/11/2019"));
        listJobdesk.add(new Jobdesk("Lembaga Polda Banten","1/11/2019"));
    }

    public static ItemTwoFragment newInstance(){
        ItemTwoFragment fragment = new ItemTwoFragment();
        return fragment;
    }
}
