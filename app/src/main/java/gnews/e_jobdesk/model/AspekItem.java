package gnews.e_jobdesk.model;

import com.google.gson.annotations.SerializedName;

public class AspekItem{

	@SerializedName("aspek_domain_id")
	private int aspekDomainId;

	@SerializedName("updated_at")
	private Object updatedAt;

	@SerializedName("order_number")
	private int orderNumber;

	@SerializedName("aspek_name")
	private String aspekName;

	@SerializedName("aspek_description")
	private String aspekDescription;

	@SerializedName("created_at")
	private Object createdAt;

	@SerializedName("id")
	private int id;

	public void setAspekDomainId(int aspekDomainId){
		this.aspekDomainId = aspekDomainId;
	}

	public int getAspekDomainId(){
		return aspekDomainId;
	}

	public void setUpdatedAt(Object updatedAt){
		this.updatedAt = updatedAt;
	}

	public Object getUpdatedAt(){
		return updatedAt;
	}

	public void setOrderNumber(int orderNumber){
		this.orderNumber = orderNumber;
	}

	public int getOrderNumber(){
		return orderNumber;
	}

	public void setAspekName(String aspekName){
		this.aspekName = aspekName;
	}

	public String getAspekName(){
		return aspekName;
	}

	public void setAspekDescription(String aspekDescription){
		this.aspekDescription = aspekDescription;
	}

	public String getAspekDescription(){
		return aspekDescription;
	}

	public void setCreatedAt(Object createdAt){
		this.createdAt = createdAt;
	}

	public Object getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"AspekItem{" + 
			"aspek_domain_id = '" + aspekDomainId + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",aspek_name = '" + aspekName + '\'' + 
			",aspek_description = '" + aspekDescription + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}