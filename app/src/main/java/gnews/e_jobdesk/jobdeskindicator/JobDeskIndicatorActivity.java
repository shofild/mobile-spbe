package gnews.e_jobdesk.jobdeskindicator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import gnews.e_jobdesk.R;

public class JobDeskIndicatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_desk_indicator);
    }
}
