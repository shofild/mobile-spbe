package gnews.e_jobdesk.jobdesk;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import gnews.e_jobdesk.R;

public class JobdeskDetailedActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jobdesk_detailed);

        getIncomingIntent();
    }

    private void getIncomingIntent(){
        if (getIntent().hasExtra("title") && getIntent().hasExtra("desc") && getIntent().hasExtra("deadline")){
            String title = getIntent().getStringExtra("title");
            String desc = getIntent().getStringExtra("desc");
            String deadline = getIntent().getStringExtra("deadline");

            setText1(title,desc,deadline);
        }
    }

    private void setText1(String title, String desc, String deadline){
        TextView otitle = findViewById(R.id.title);
        otitle.setText(title);
        TextView odesc = findViewById(R.id.desc);
        odesc.setText(desc);
        TextView odeadline = findViewById(R.id.deadline);
        odeadline.setText(deadline);
    }
}
