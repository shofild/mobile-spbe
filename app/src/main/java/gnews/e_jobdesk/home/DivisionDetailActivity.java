package gnews.e_jobdesk.home;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import gnews.e_jobdesk.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DivisionDetailActivity extends AppCompatActivity {
    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_division_detail);
        String division_name = getIntent().getStringExtra("division_name");
        getSupportActionBar().setTitle(division_name);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        expListView = findViewById(R.id.exp_list);

        initData();

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                Toast.makeText(DivisionDetailActivity.this, "TEST"+childPosition, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(DivisionDetailActivity.this, TaskDetail.class);
                startActivity(intent);
                return false;
            }
        });
    }

    private void initData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        //data Header
        listDataHeader.add("DONE");
        listDataHeader.add("TO DO");

        //data Child
        List<String> taskDone = new ArrayList<String>();
        taskDone.add("Membuat website");
        taskDone.add("Menambah laporan revisi");
        taskDone.add("Cetak koran");

        List<String> taskToDo = new ArrayList<String>();
        taskToDo.add("Upload Berita bulan ini");
        taskToDo.add("Revisi anggaran");

        listDataChild.put(listDataHeader.get(0), taskDone);
        listDataChild.put(listDataHeader.get(1), taskToDo);
    }
}
