package gnews.e_jobdesk.model;

import com.google.gson.annotations.SerializedName;

public class DomainItem{

	@SerializedName("domain_name")
	private String domainName;

	@SerializedName("updated_at")
	private Object updatedAt;

	@SerializedName("order_number")
	private int orderNumber;

	@SerializedName("domain_model_id")
	private int domainModelId;

	@SerializedName("domain_description")
	private String domainDescription;

	@SerializedName("created_at")
	private Object createdAt;

	@SerializedName("id")
	private int id;

	public void setDomainName(String domainName){
		this.domainName = domainName;
	}

	public String getDomainName(){
		return domainName;
	}

	public void setUpdatedAt(Object updatedAt){
		this.updatedAt = updatedAt;
	}

	public Object getUpdatedAt(){
		return updatedAt;
	}

	public void setOrderNumber(int orderNumber){
		this.orderNumber = orderNumber;
	}

	public int getOrderNumber(){
		return orderNumber;
	}

	public void setDomainModelId(int domainModelId){
		this.domainModelId = domainModelId;
	}

	public int getDomainModelId(){
		return domainModelId;
	}

	public void setDomainDescription(String domainDescription){
		this.domainDescription = domainDescription;
	}

	public String getDomainDescription(){
		return domainDescription;
	}

	public void setCreatedAt(Object createdAt){
		this.createdAt = createdAt;
	}

	public Object getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"DomainItem{" + 
			"domain_name = '" + domainName + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",order_number = '" + orderNumber + '\'' + 
			",domain_model_id = '" + domainModelId + '\'' + 
			",domain_description = '" + domainDescription + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}