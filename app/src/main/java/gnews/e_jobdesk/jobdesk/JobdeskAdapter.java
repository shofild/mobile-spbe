package gnews.e_jobdesk.jobdesk;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import gnews.e_jobdesk.R;
import gnews.e_jobdesk.jobdeskdomain.JobDeskDomainActivity;

public class JobdeskAdapter extends RecyclerView.Adapter<JobdeskAdapter.MyViewHolder>{
    Context mContext;
    List<Jobdesk> mData;

    public JobdeskAdapter(Context mContext, List<Jobdesk> mData) {
        this.mContext = mContext;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.list_job_desk,parent,false);
        MyViewHolder vHolder = new MyViewHolder(v);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.tv_title.setText(mData.get(position).getTitle());
        holder.tv_deadline.setText(mData.get(position).getDeadline());

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, JobDeskDomainActivity.class);
                intent.putExtra("title",mData.get(position).getTitle());
                intent.putExtra("desc",mData.get(position).getDesc());
                intent.putExtra("deadline",mData.get(position).getDeadline());
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView tv_title;
        private TextView tv_deadline;
        private CardView parentLayout;

        public MyViewHolder(View itemView){
            super(itemView);


            tv_title = (TextView) itemView.findViewById(R.id.tv_location);
            tv_deadline = (TextView) itemView.findViewById(R.id.tv_deadline);
            parentLayout = (CardView) itemView.findViewById(R.id.cv_jobdesk);
        }
    }
}
